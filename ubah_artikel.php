<?php

// mengaktifkan session
session_start();

include 'koneksi.php';
$username = $_SESSION["username"];

$id = $_GET['id'];


$sql = "SELECT * FROM artikel WHERE id='$id'";
$result = $koneksi->query($sql);
$hasil = $result->fetch_assoc();

$sql2 = "SELECT * FROM users WHERE username='$username'";
$result2 = $koneksi->query($sql2);
$hasil2 = $result2->fetch_assoc();

// untuk mencegah user langsung pergi ke home.php tanpa login
if ($_SESSION["login"] !== 1) {
    header("Location:index.php?pesan=login");
}

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

    <!-- Css Ku-->
    <style>
        @font-face {
            font-family: quicksand;
            src: url(Font/Quicksand-Medium.ttf);
        }

        * {
            font-family: quicksand;

        }
    </style>
    <title>My Blog</title>
</head>

<body>

    <!-- Navbar -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
        <div class="container">
            <a class="navbar-brand " href="home.php" style="font-weight: bold;">
                <i style="font-size: 23px; color: rgb(245, 245, 245);"></i> <?php echo $hasil2["username"]; ?>
                Website</a>

            <div class="navbar-nav">
                <a class="nav-link ml-3" style="font-size:18px; " href="home.php">Home </a>
                <a class="nav-link " style="font-size:18px; " href="akun.php">Akun <span class="sr-only">(current)</span></a>
                <a class="nav-link " style="font-size:18px; " href="admin.php">Admin</a>
                <a class="nav-link active " style="font-size:18px; " href="artikel.php">Artikel</a>

            </div>

            <div class="ml-auto navbar-nav">
                <a type="button" style="width:110px;" class="btn btn-success " href="logout.php">Log out</a>
            </div>
        </div>
    </nav>
    <!-- Navbar End-->

    <!-- Form Ubah Artikel-->

    <div class="container" style="margin-top:120px; font-size:15px;">
        <h1 style="font-weight:bold;"> Ubah Artikel</h1>
        <form method="POST" action="proses_ubah_artikel.php">

            <input type="hidden" name="id" value="<?= $id ?>">

            <div class="mb-4 mt-3">
                <label class="form-label">Judul</label>
                <input value="<?= $hasil['title'] ?>" rows="3" type="text" name="title" class="form-control">
            </div>

            <div class="mb-4">
                <label class="form-label">Konten</label>
                <textarea rows="7" name="contents" class="form-control"><?= $hasil['contents'] ?></textarea>
            </div>

            <a class="btn btn-outline-primary" href="artikel.php"> <svg xmlns="http://www.w3.org/2000/svg" width="17" height="17" fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                    <path fill-rule="evenodd" d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
                </svg> Kembali</a>
            <button type="submit" class="btn btn-primary" style="float: right;"> Submit</button>

        </form>
    </div>

    <!-- Form Ubah Artikel End -->



    <!-- Footer -->

    <div style="margin-top: 160px;">
        <footer style="font-weight: 100;" class="bg-dark text-white">
            <div class="container">
                <div class="row pt-3">
                    <div class="col text-center">
                        <p> &copy; Copyright By <?php echo $hasil2["nama"]; ?></p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
    <!-- Footer End -->

    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>
</body>

</html>